class FizzBuzz
  def self.generate(num)
    if !(num.kind_of?(Integer)) || num <= 0
      raise ArgumentError.new('argument should be kind of Integer')
    end

    result = ''
    result = 'Fizz' if num % 3 == 0
    result = result + 'Buzz' if num % 5 == 0
    result = num.to_s if result.empty?
    result
  end
end
