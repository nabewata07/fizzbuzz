require './fizzbuzz'

describe FizzBuzz do
  describe "#generate" do
    let (:message) { 'argument should be kind of Integer' }
    subject { FizzBuzz }

    # 先にやる
    context 'Integer以外が入力されたとき' do
      it "'hogehoge'が入力されたとき" do
        expect{ subject.generate('hogehoge') }.
          to raise_error(ArgumentError, message)
      end
    end

    context '0以下の数値が入力されたとき' do
      it '0が入力されたときArgumentError' do
        expect{ subject.generate(0) }.
          to raise_error(ArgumentError, message)
      end
      it '-1が入力されたときArgumentError' do
        expect{ subject.generate(-1) }.
          to raise_error(ArgumentError, message)
      end
    end

    context '入力が3の倍数であり5の倍数でないとき' do
      it "3が入力されたとき'Fizz'を返す" do
        expect(subject.generate(3)).to eq('Fizz')
      end
      it "6が入力されたとき'Fizz'を返す" do
        expect(subject.generate(6)).to eq('Fizz')
      end
    end

    context '入力が5の倍数であり3の倍数でないとき' do
      it "5が入力されたとき'Buzz'を返す" do
        expect(subject.generate(5)).to eq('Buzz')
      end
      it "10が入力されたとき'Buzz'を返す" do
        expect(subject.generate(10)).to eq('Buzz')
      end
      it "100が入力されたとき'Buzz'を返す" do
        expect(subject.generate(100)).to eq('Buzz')
      end
    end

    context '入力が5の倍数でありかつ3の倍数であるとき' do
      it "15が入力されたとき'FizzBuzz'を返す" do
        expect(subject.generate(15)).to eq('FizzBuzz')
      end
      it "30が入力されたとき'FizzBuzz'を返す" do
        expect(subject.generate(30)).to eq('FizzBuzz')
      end
    end

    context '入力が3の倍数でも5の倍数でもないとき' do
      it "1が入力されたとき'1'を返す" do
        expect(subject.generate(1)).to eq('1')
      end
      it "2が入力されたとき'2'を返す" do
        expect(subject.generate(2)).to eq('2')
      end
      it "4が入力されたとき'4'を返す" do
        expect(subject.generate(4)).to eq('4')
      end
    end

  end
end
